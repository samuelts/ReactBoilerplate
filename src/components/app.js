import React, { Component } from 'react';
import ClassComp from './class-comp';
import FuncComp from './func-comp';

export default class App extends Component {
  render() {
    return (
      <div className='container'>
        <ClassComp />
        <FuncComp />
      </div>
    );
  }
}
